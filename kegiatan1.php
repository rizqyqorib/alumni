<?php
	include('templates/header.php');
?>
<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>	
<!--BERITA-->
	<div class="container" style="margin-top: 100px;">
		<div class="col-sm-8" style="margin-top: 20px;">
			<div class="box" style="padding: 50px;">
								<h2>Sentika : Universitas Islam Indonesia</h2>
								<hr>
			<br>
					<center><img src="seminar1.png" width="300px" height="150px" alt="..."></center>
							<br>
						<p>Seminar Nasional (SEMNAS) Informatika UII adalah sebuah seminar yang 
						menggabungkan tiga buah seminar yang sebelumnya dilaksanakan secara terpisah 
						oleh Jurusan Teknik Informatika UII yaitu : Seminar Nasional Aplikasi Teknologi 
						Informasi (SNATi), Seminar Nasional Informatika Medis (SNIMed), dan Hacking 
						and Digital Forensics Exposed (H@dfex).</p>
							<br>
						<p>Tahun 2017 ini merupakan kedua kalinya ketiga seminar tersebut dilaksanakan 
						secara bersamaan dengan nama Seminar Nasional Informatika UII. Dengan penyatuan 
						ketiga seminar tersebut diharapkan dapat menghemat sumber daya dan pembiayaan 
						kegiatan, tetapi tetap memberikan pelayanan terbaik bagi peserta. Selain itu 
						peserta salah satu seminar juga mendapatkan kesempatan untuk bisa melihat 
						pelaksanaan dari seminar lainnya.</p>	
							<br>
						<p>Tahun ini tema untuk masing-masing seminar adalah “Internet Of Things Untuk 
						Pembangunan Kota Cerdas” untuk SNATI, “Ontology Sebagai Pendukung dalam Pemberian 
						Terapi Medis yang Tepat” untuk SNIMed, dan “Explore The New Cyber Threat Paradigm” 
						untuk H@dfex. Tahun ini SEMNAS akan dilaksanakan di Grand Mercure Hotel Yogyakarta 
						pada tanggal 5 Agustus 2017.</p>
							<br>
						<p>untuk info lebih lanjut, kalian bisa langsung menghubungi website nya.
						<a href="http://seminar.informatics.uii.ac.id/">Disini</a></p>
			
						<hr>
					
			</div>
		</div>		
<!-- RIGHT PLACE -->	
		<div class="col-sm-4" style="margin-top: 20px;">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="bea1.html">Panduan Beasiswa LPDP 2017</a></li>
					<li><a href="seminar1.html">Sentika : Universitas Islam Indonesia</a></li>
					<li><a href="loker1.html">Lowongan PT. Industri Kereta Api</a></li>
					<li><a href="loker2.html">Pertamina membuka kembali lowongan</a></li>
					<li><a href="loker3.html">Lowongan Global One Solusindo</a></li>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>
	</div>
<?php
	include('templates/footer.php');
?>
