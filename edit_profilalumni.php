
<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="icon" href="../../favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <!--border-->
    <nav class="navbar navbar-inverse navbar-fixed-top"><!--WARNA HITAM-->
      <div class="container">
        <div class="navbar-header">
			<div class="row">
				<div class="col-sm-12 text-left">
					<a class="navbar-brand" href="home.html">Home</a>
				</div>
			</div>		
        </div>
    </div>
    </nav>
<!--kontak-->
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-left">
				<h2>Data Alumni</h2>
				<hr>
			</div>
		</div>
		
			<div class="col-sm-offset-3 col-sm-6 col-sm-offset-3">
				
				<?php 
				include "koneksi.php";
				$id_alumni = $_GET['id_alumni'];
				$query_mysql = mysql_query("SELECT * FROM alumnitelkom WHERE id_alumni='$id_alumni'")or die(mysql_error());
				while($data = mysql_fetch_array($query_mysql)){
				?>
				
				<form action="update.php" method="post">
				<h3 class="text-center">Edit Data Alumni</h3><br>
				
				<div class="form-group">
				<input type="hidden" name="id_alumni" value="<?php echo $data['id_alumni'] ?>">
					<label for="nama">Nama</label>
					<input type="text" name="nama" class="form-control" value="<?php echo $data['nama'] ?>"/>
				</div>
				<div class="form-group">
					<label for="nama">Prodi</label>
					<input type="text" name="jurusan" class="form-control" value="<?php echo $data['jurusan'] ?>"/>
				</div>
				<div class="form-group">
					<label for="nama">Status</label>
					<input type="text" name="status" class="form-control" value="<?php echo $data['status'] ?>"/>
				</div>
				<div class="form-group">
					<label for="nama">Angkatan</label>
					<input type="text" name="Angkatan" class="form-control" value="<?php echo $data['angkatan'] ?>"/>
				</div>
				<div class="form-group">
					<label for="nama">Lokasi</label>
					<input type="text" name="lokasi" class="form-control" value="<?php echo $data['lokasi'] ?>"/>
				</div>
				<div class="form-group">
					<label for="nama">Perusahaan</label>
					<input type="text" name="perusahaan" class="form-control" value="<?php echo $data['perusahaan'] ?>"/>
				</div>
				<div class="top-margin">
				<div class="text-right">
				<center><input class="btn btn-success" type="submit" name="submit" value="Simpan">
				</div>
				</div>								
				</form>
				<?php } ?>
				</div>
			</div>
			
		
        
              <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>