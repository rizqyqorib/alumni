<?php
	include('templates/header.php');
?>
<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>	
<!--BERITA-->
	<div class="container" style="margin-top: 100px;">
		<div class="col-sm-8" style="margin-top: 20px;">
			<div class="box" style="padding: 50px;">
				<ol class="breadcrumb">
				<li><a href="home.php">Home</a></li>
				<li><a href="bea.php">Beasiswa</a></li>
				<li class="active">Beasiswa Unggulan Masyarakat Berprestasi</li>
			</ol>
								<h2>Beasiswa Unggulan Masyarakat Berprestasi untuk S1, S2, S3, dan Non Degree</h2>
								<hr>
			<br>
						<p>Beasiswa Unggulan Kementerian Pendidikan dan Kebudayaan (Kemdikbud) memiliki beragam variasi beasiswa. Selain Beasiswa Unggulan Pegawai Kemdikbud. Yang satu ini tak kalah menariknya. Beasiswa Unggulan Masyarakat Berprestasi. Namanya lebih spesifik untuk membedakan Beasiswa Unggulan yang satu dengan lainnya. </p>
							<br>
						<p>Beasiswa Unggulan Masyarakat Berprestasi ditujukan bagi siswa/mahasiswa berprestasi, guru berprestasi, peraih medali olimpiade internasional, juara tingkat nasional atau internasional bidang sains, teknologi, seni budaya, dan olahraga, pegawai/karyawan berprestasi yang mendapatkan persetujuan dan diusulkan oleh atasan, seniman, serta pegiat sosial.</p>
							<br>
						<p>Selain beasiswa degree untuk program gelar S1, S2, dan S3. Beasiswa Unggulan Masyarakat Berprestasi juga menyediakan beasiswa non-degree. Beasiswa non gelar ini diperuntukkan bagi guru, tenaga kependidikan pada tingkat PAUD sampai dengan SMA/SMK, pegiat budaya, seniman dan pegiat sosial kecuali dosen untuk mengikuti program residensi, menjadi pembicara dalam workshop atau konferensi, utamanya bidang pendidikan dan kebudayaan.</p>
							<br>
						<p>Pelamar yang berminat dapat memanfaatkan Beasiswa Unggulan Berprestasi untuk melanjutkan studi jenjang sarjana (S1), magister (S2), dan doktor (S3) di dalam negeri atau mengikuti program non degree yang diminati. Yang menarik dari beasiswa ini, pelamar bisa berasal dari mahasiswa baru atau mahasiswa yang sedang menjalani perkuliahan (on going). Beasiswa yang diberikan untuk program gelar S1, S2, dan S3 dapat berupa biaya pendidikan (tuition fee), biaya hidup, dan biaya buku. </p>
						<br>
						<p>simak juga <a href="http://www.beasiswapascasarjana.com/2017/06/pendaftaran-beasiswa.html">Pendaftaran Beasiswa 2018 - 2019 untuk S1, S2, S3</a></p>			
							<br>
						
			</div>
		</div>		
<!-- RIGHT PLACE -->	
		<div class="col-sm-4" style="margin-top: 20px;">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="bea1.html">Panduan Beasiswa LPDP 2017</a></li>
					<li><a href="seminar1.html">Sentika : Universitas Islam Indonesia</a></li>
					<li><a href="loker1.html">Lowongan PT. Industri Kereta Api</a></li>
					<li><a href="loker2.html">Pertamina membuka kembali lowongan</a></li>
					<li><a href="loker3.html">Lowongan Global One Solusindo</a></li>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>
	</div>
<?php
	include('templates/footer.php');
?>
