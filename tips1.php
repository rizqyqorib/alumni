<?php
	include('templates/header.php');
?>

<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>
	<div class="container" style="margin-top: 100px;">
		<div class="row">
<!--profil alumni-->
			<div class="col-sm-8" style="margin-top: 20px;">
				<div class="box" style="padding:50px;">
					<ol class="breadcrumb">
				<li><a href="home.php">Home</a></li>
				<li><a href="tips.php">Tips-tips</a></li>
				<li class="active">3 Tips Agar Anda Betah Kerja di Kantor</li>
			</ol>
			<h2 style="margin-bottom: 50px;">3 Tips Agar Anda Betah Kerja di Kantor</h2>
					<center><img src="tips.png" class="img-responsive" alt="..."></center>
							<br>
							<br>
						<p>Tidak mudah untuk membuat rasa nyaman pada sebuah pekerjaan hal itu yang harus 
						Anda pikiran saat Anda menjalani pekerjaan yang Anda jalani sekarang.</p>
							<br>
						<p>Agar Anda bisa bertahan lama pada pekerjaan Anda dan bisa menikmatinya dengan maksimal, 
						dan membantu Anda dalam meningkatkan kualitas pekerjaan. Untuk bisa membuat rasa nyaman 
						banyak yang harus Anda pelajari.</p>	
							<br>
						<p>Dan di bawah ini ada beberapa cara yang akan membantu Anda untuk membuat rasa 
						nyaman pada pekerjaan yang Anda jalani sekarang:</p>
							<br>
						<p><b>1.Lokasi tempat tinggal</b></p>
							<br>
						<p>Lokasi tempat tinggal yang jauh dari kantor membutuhkan usaha yang keras untuk 
						berangkat ke kantor dan pulang ke rumah. Jika terlalu jauh ditakutkan bisa membuat 
						badan Anda capek ketika sampai di kantor dan ambruk ketika sampai di rumah.</p>	
							<br>
						<p>Mungkin dengan memilih lokasi tempat tinggal yang hanya beberapa langkah dari tempat 
						kerja dapat membuat Anda hidup lebih segar tanpa harus memikirkan perjalanan panjang yang 
						harus ditempuh setiap harinya. Itu akan membuat Anda lebih bisa fokus dalam bekerja tanpa 
						harus memikirkan rasa lelah dalam perjalanan.</p>	
							<br>
						<p><b>2.Rekan kerja seperti keluarga</b></p>
							<br>
						<p>Jika teman-teman dan atasan di kantor sudah Anda anggap seperti keluarga sendiri, 
						maka Anda bekerja bisa jadi nyaman serta akan saling membantu dan mendukung jika 
						mengalami kendala-kendala dalam bekerja.</p>
							<br>
						<p>Usahakan hubungan anda dengan bos atau atasan Anda akrab dan tidak saling jaim 
						(jaga image) seperti teman atau orang tua, namun Anda tetap hormat kepadanya saat 
						atasan memberikan intruksi atau sedang memberikan masukan atas pekerjaan Anda.</p>
							<br>
						<p><b>3.Ibadah</b></p>
							<br>
						<p>Jangan lupa menjalankan ibadah atau berdo saat Anda menjalankan berbagai tugas 
						kantor Jika anda bekerja ikhlas maka Anda bekerja akan tenang, nyaman, damai, 
						tentram dan tanpa beban.</p>	
							<br>
						<p>Itulah beberapa hal yang bisa Anda lakukan untuk membuat Anda nyaman dengan 
						pekerjaan yang Anda lakukan. Sehingga itu tidak mengganggu kualitas Anda dalam 
						bekerja, dan membantu Anda untuk bertahan pada pekerjaan yang Anda lakukan sekarang.</p>	
							<br>
						<p>Ingin mengetahui tips karir dan bisnis yang bermanfaat lainnya? Simak di <a href="http://laruno.com/">Laruno.com</a></p>
				</div>	
			</div>
		<div class="col-sm-4" style="margin-top: 20px;">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="bea1.html">Panduan Beasiswa LPDP 2017</a></li>
					<li><a href="seminar1.html">Sentika : Universitas Islam Indonesia</a></li>
					<li><a href="loker1.html">Lowongan PT. Industri Kereta Api</a></li>
					<li><a href="loker2.html">Pertamina membuka kembali lowongan</a></li>
					<li><a href="loker3.html">Lowongan Global One Solusindo</a></li>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>
		</div>
	</div>
			<br>
			<br>
<?php
	include('templates/footer.php');
?>
