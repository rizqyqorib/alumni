<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Portal Alumni - Daftar Alumni</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

<!--NAVBAR-->
 <div class="navbar navbar-default navbar-fixed-top" >
	 <div class="container">
		  <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
		  </div>
				<div class="col-md-7">
				<a class="navbar-brand" href="home.html"><img src="logo.png" width="150" height="55"></a>
					  <ul class="nav navbar-nav navbar-left">
						<li><a href="home.html"><b>Beranda</b></a></li>
						<li><a href="tentang_kami.html"><b>Tentang Kami</b></a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown"><b>Berita</b><b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="bea.html">Beasiswa</a></li>
									<li><a href="kegiatan.html">Kegiatan</a></li>
									<li><a href="tips.html">Tips</a></li>
								</ul>
						</li>
						<li><a href="loker.html"><b>Lowongan</b></a></li>
					  </ul>	
				</div>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="login.html"><b>Login</b></a></li>
			</ul>	
	  </div>
 </div>
<br>

<!--JUDUL HALAMAN-->
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-left">
				<h2><b>Daftar Alumni</b></h2>
					<hr>
			</div>
		</div>
	</div>	
		
	
<!--BERITA-->
	<div class="container">
		<div class="col-sm-8">
			<div class="col-sm-6">	
				<form action="simpanuser.php" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label for="nim">Nim</label>
						<input type="text" name="nim" class="form-control" placeholder="Nim">
					</div>
			</div>			
			<div class="col-sm-6">			
						<div class="form-group">
							<label for="nama">Nama</label>
							<input type="text" name="nama" class="form-control" placeholder="Nama">
						</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
								<label for="sel1">Jurusan</label>
									<select class="form-control" id="sel1" name="jurusan">
										<option>S1 Teknik Informatika</option>
										<option>S1 Teknik Telekomunikasi</option>
										<option>D3 Teknik Telekomunikasi</option>
										<option>S1 Sistem Informasi</option>
										<option>S1 Rekayasa Perangkat Lunak</option>
										<option>S1 DKV</option>
										<option>S1 Teknik Industri</option>
									</select>
				</div>
			</div>
			<div class="col-sm-6">	
				<div class="form-group">
								<label for="sel1">Status</label>
									<select class="form-control" id="sel1" name="status">
										<option>Belum bekerja</option>
										<option>Bekerja</option>
										<option>Lanjut Studi</option>
										<option>wirausaha</option>
									</select>
				</div>
			</div>					
			<div class="col-sm-6">			
						<div class="form-group">
							<label for="lokasi">Lokasi</label>
							<input type="text" name="lokasi" class="form-control" placeholder="Lokasi">
						</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
								<label for="sel1">Angkatan</label>
									<select class="form-control" id="sel1" name="angkatan">
										<option>2010</option>
										<option>2011</option>
										<option>2012</option>
										<option>2013</option>
										<option>2014</option>
										<option>2015</option>
										<option>2016</option>
										<option>2017</option>
									</select>
				</div>
			</div>	
			<div class="col-sm-6">			
						<div class="form-group">
							<label for="skripsi">Judul Skripsi</label>
							<input type="text" name="skripsi" class="form-control" placeholder="Judul Skripsi">
						</div>
			</div>
			<div class="col-sm-12">	
					<button type="submit" name="submit">Kirim</button>
			</div>	
		</div>
	</form>

			<div class="col-sm-4">
		<h4><b><font color="#c0392b">Artikel Terbaru</font></b></h4>
			<hr>
			<h4><a href="bea1.html"> <font color="#95a5a6">Panduan Beasiswa LPDP 2017</font></a></h4>
			<br>
			<h4><a href="seminar1.html"> <font color="#95a5a6">Sentika : Universitas Islam Indonesia</font></a></h4>
			<br>
			<h4><a href="loker1.html"> <font color="#95a5a6">Lowongan PT. Industri Kereta Api</font></a></h4>
			<br>
			<h4><a href="loker2.html"> <font color="#95a5a6">Pertamina membuka kembali lowongan</font></a></h4>
			<br>
			<h4><a href="loker3.html"> <font color="#95a5a6">Lowongan Global One Solusindo</font></a></h4>
			<br>
		</div>
		<div class="col-sm-4">
			<h4><b><font color="#c0392b">Quick Link</font></b></h4>
			<hr>
			<h4><a href="carialumni1.php"> <font color="#95a5a6">Pencarian Alumni</font></a></h4>
			<br>
			<h4><a href="tambah.html"> <font color="#95a5a6">Tambahkan Berita</font></a></h4>
		</div>
	</div>
<!-- RIGHT PLACE -->
<br>
<br>
	
<!-- FOOTER -->
<section>
	
<div style="width:1348px; height:300px; background:#e62129; padding:10px; color:#white">
<div class="container">	
	<div class="col-sm-3">
	<br>
			<h4><b><font color="white">Alumni Kampus ITT Purwokerto</b></h4>

					Jl. D. I. Panjaitan No. 128<br>
					Purwokerto 53147<br>
					Telp. 0281-641629<br>
					Faks. 0281-641630
					</font>
		<br>
		<br>
		<img src="logo1.png" width="100" height="90">		
		
			
	</div>
	
	<div class="col-sm-2  col-sm-offset-1">
		<font color="white">
		<br>
			<h4><b>Tentang Kami</b></h4>
				<br>
				<a href="tentang_kami.html">
							<h5 class="media-heading"><font color="white">ITT Purwokerto</font></h5>
						</a><br>
				<a href="tentang_kami.html">
							<h5 class="media-heading"><font color="white">Alumni</font></h5>
						</a><br>
				<a href="tentang_kami.html">
							<h5 class="media-heading"><font color="white">CDC</font></h5>
						</a>		
		</font>					
	</div>
		<div class="col-sm-2">
		<font color="white">
		<br>
			<h4><b>Berita</b></h4>
				<br>
				<a href="loker.html">
							<h5 class="media-heading"><font color="white">Lowongan</font></h5>
						</a><br>
				<a href="bea.html">
							<h5 class="media-heading"><font color="white">Beasiswa</font></h5>
						</a><br>
				<a href="kegiatan.html">
							<h5 class="media-heading"><font color="white">Kegiatan</font></h5>
						</a><br>
				<a href="tips.html">
							<h5 class="media-heading"><font color="white">Tips</font></h5>
						</a
		</font>					
	</div>
		<div class="col-sm-2">
		<font color="white">
		<br>
			<h4><b>Lainnya</b></h4>
				<br>
				<a href="carialumni1.php">
							<h5 class="media-heading"><font color="white">Pencarian alumni</font></h5>
						</a><br>
				<a href="kegiatan.html">
							<h5 class="media-heading"><font color="white">Seminar</font></h5>
						</a><br>
				<a href="tambah.html">
							<h5 class="media-heading"><font color="white">Tambah berita</font></h5>
						</a><br>
				<a href="tentang_kami.html">
							<h5 class="media-heading"><font color="white">Cerita alumni</font></h5>
						</a><br>
		</font>					
	</div>
		
				<div class="col-sm-2">
					
				</div>
			</div>	
		</div>

<div style="width:1348px; height:50px; background:#5f5d5d; padding:10px; color:#white">
	<div class="container">
		<p><center>@copyright Rizqy F. Qorib</center></p>
	</div>	
</div>
</section>
				
        
              <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="../../js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
