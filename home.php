<?php 
	include('templates/header.php');
?>

<div class="home-1">
	<div class="container text-center">
		<div class="row">
			<div class="col-lg-12">
				<h1 style="margin-top: 15%; margin-bottom: 4%; font-family: 'Poppins', sans-serif;" class="bold">Temukan Lebih Banyak <br/> Alumni Disini</h1>
				<p style="margin-bottom: 4%; color: #fff; font-size: 0.9em;">Website portal alumni adalah sebuah website yang dirancang <br>oleh unit kerja CDC ITTP. Website ini menyediakan Beberapa informasi<br>  seperti loker, beasiswa, dan kegiatan lainnya yang ditujukan untuk alumni ITTP.<br> Sebuah website yang diharapkan dapat berfungsi sebagai penunjang interaksi sesama alumni<br> untuk saling berbagi informasi, pengalaman maupun cerita.</p>
				<a href="carialumni1.php" class="button">Cari Alumni</a>
			</div>
		</div>
	</div>
</div>
			
<div class="home-2">
	<div class="container">
		<h1 class="text-center judul" style="margin-bottom: 25px;">Berita terbaru</h1>
		<div style="border-bottom: 3px solid #E62129; width: 5%; margin:0 auto; margin-bottom: 50px;"></div>
		<p  style="margin-bottom: 100px;" class="text-center">
		<div class="row">

		<?php
			include ("koneksi.php");
			$query = "Select * from saran order by id desc limit 3";
			$data = mysql_query($query);
			while($hasil = mysql_fetch_array($data)){
		?>

			<div class="col-lg-4">
				<div class="box">
					<img src="dist/img/<?php echo $hasil['foto']; ?>" class="img-responsive box-1">
					<div class="row">
						<div class="col-lg-6 text-left home-3"> 
							<p style="font-size: 0.8em;"><i class="material-icons">access_time</i> 20 Maret 2019</p>
						</div>
						<div class="col-lg-6 text-right home-3">
							<p style="font-size: 0.8em;"><i class="material-icons">chat_bubble_outline</i> 0</p>
						</div>
						<div class="col-lg-12">
							<h4 class="media-heading">
								<?php 
								echo (strlen($hasil['judul']) > 40) ? substr($hasil['judul'],0,40).'...' : $hasil['judul'];
								?>
							</h4>
						</div>	
						<div class="col-lg-6">

						</div>
						<div class="col-lg-6 text-right">
							<a href="detailberita.php?id=<?php echo $hasil['id']; ?>" class="btn button-12">Lebih Lengkap</a>
						</div>
					</div>
					<p class="huhu"><?php echo $hasil['kategori']; ?></p>
					
				</div>
			</div>

		<?php } ?>

	</div>
	</div>
</div>

<!-- LEAVE MESSAGE -->
<br>
	<div class="container">

		<div class="row">
		<div class="col-sm-12">
			<h1 class="text-center judul" style="margin-bottom: 25px;">Kritik	& Saran</h1>
			<div style="border-bottom: 3px solid #E62129; width: 5%; margin:0 auto; margin-bottom: 50px;"></div>
			<p  style="margin-bottom: 50px;" class="text-center">Tambahkan kritik & saran untuk website ini agar kedepannya <br>bisa menjadi website yang lebih baik lagi sesuai dengan apa yang dibutuhkan alumni. <br>Tambahkan juga informasi lainnya seperti info loker, beasiswa, pelatihan<br> maupun kegiatan alumni yang anda ketahui</p>
			<div class="text-center">
				<a href="tambah.php"><button class="button">Tambah Berita</button></a>
				<a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><button class="button">Kritik & Saran</button></a>
			</div>
			<div class="collapse" id="collapseExample">
				<form action="proses_saran.php" method="POST" class="saran">
					<div class="form-group">
						<input type="text" name="nama" class="form-control" placeholder="Nama">
					</div>
					<div class="form-group">
						<input type="text" name="email" class="form-control" placeholder="Email">
					</div>
					<div class="form-group">
						<textarea class="form-control" name="saran" rows="3" id="comment">	</textarea>
					</div>
					<button type="submit" class="btn button" style="width: 100%; height: 50px;">Submit</button>
				</form>
			</div>
		</div>
		</div>
	</div>
</div>
<br>
<br><div class='row box' style="padding-top: 60px;">
	    <div class='col-md-12'>
	    	<div class="container">
	      <div class="row text-center">
	    <h2 style="margin-bottom: 15px;">Testimoni</h2>
	      <div class="carousel slide media-carousel" id="media">
	        <div class="carousel-inner">
	          <div class="item  active">
	            <div class="row text-center">
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div>          
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div>
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div> 
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div> 
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div> 
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div> 	              	              	                     
	            </div>
	          </div>
	          <div class="item">
	            <div class="row">
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div>          
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div>
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div> 
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>	              </div> 
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div> 
	              <div class="col-md-2">
	                <a class="thumbnail" href="#">
	                	<img alt="" src="http://placehold.it/150x150">
	                	<p>kritik & saran</p>
	                </a>
	              </div> 	              	              	                     
	            </div>
	          </div>
	        </div>
	        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
	        <a data-slide="next" href="#media" class="right carousel-control">›</a>
	      </div>                          
	    </div>
	  </div>

	</div>
</div>
</div>

<?php
	include('templates/footer.php');
?>
