<div class="home-5">
    <div class="container"> 
        <div class="col-lg-5 style-footer">
            <h4><b>Alumni Kampus ITT Purwokerto</b></h4>
                <ul class="list-unstyled">
                  <li>Jl. D. I. Panjaitan No. 128</li>
                  <li>Purwokerto 53147</li>
                  <li>Telp. 0281-641629</li>
                  <li>Faks. 0281-641630</li>
                </ul>
            <p>@copyright Rizqy F. Qorib</p>
            <!-- <img src="logo1.png" width="100" height="90">   -->        
        </div>
        
        <div class="col-lg-2 style-footer">
            <h4><b>Tentang Kami</b></h4>
            <ul class="list-unstyled">
                <li><a href="http://ittelkom-pwt.ac.id/" target="_blank">ITT Purwokerto</a></li>
                <li><a href="tentang_kami.html">Alumni</a></li>
                <li><a href="tentang_kami.html">CDC</a>
            </ul>                                       
        </div>
        <div class="col-lg-2 style-footer">
            <h4><b>Berita</b></h4>
                <ul class="list-unstyled">
                    <li><a href="loker.html">Lowongan</a></li>
                    <li><a href="bea.html">Beasiswa</a></li>
                    <li><a href="kegiatan.html">Kegiatan</a></li>
                    <li><a href="tips.html">Tips</a></li>
                </ul>                           
        </div>
        <div class="col-lg-2 style-footer">
            <h4><b>Lainnya</b></h4>
            <ul class="list-unstyled">
                <li><a href="carialumni1.php">Pencarian alumni</a></li>
                <li><a href="kegiatan.html">Seminar</a></li>
                <li><a href="tambah.html">Tambah berita</a></li>
                <li><a href="tentang_kami.html">Cerita alumni</a></li>
            </ul>                   
        </div>
    </div>  
</div>
      
              <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../dist/custom/js/dalam.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>