<?php
	include('templates/header.php');
?>
	<div class="container-fluid" style="margin-top: 200px;">
		
			<div class="col-sm-12">
				<div class="box" style="padding: 50px;">
				<h2>Data Alumni</h2>
				<table class="table table-responsive">
				<thead>
                <tr>
                  <th>No</th>
				  <th>Nama</th>
				  <th>Prodi</th>
				  <th>Status</th>
                  <th>Angkatan</th>
                  <th>Lokasi</th>
				  <th>Perusahaan</th>
				  <th>Opsi</th>
                </tr>
              </thead>
			  
			  <?php
			  include "templates/koneksi.php";
			  $query_mysql = mysql_query("SELECT * FROM alumnitelkom") or die (mysql_error());
			  $nomor = 1;
			  while($data = mysql_fetch_array($query_mysql)){
			  ?>
			  <tbody>
				<tr>
					<td><?php echo $nomor++; ?></td>
					<td><?php echo $data['nama']; ?></td>
					<td><?php echo $data['jurusan']; ?></td>
					<td><?php echo $data['status']; ?></td>
					<td><?php echo $data['angkatan']; ?></td>
					<td><?php echo $data['lokasi']; ?></td>
					<td><?php echo $data['perusahaan']; ?></td>
					<td>
						<a class="btn button-4 btn-sm" href="edit_alumni.php?id_alumni=<?php echo $data['id_alumni'];?>"><i class="material-icons">border_color</i></a>
						<a class="btn button-5 btn-sm" href="hapus-alumni.php?id_alumni=<?php echo $data['id_alumni'];?>"><i class="material-icons">delete</i></a>
					</td>
				</tr>
				<?php } ?>
				</tbody>
				
				</table>
				<p><center><a href="home_admin.php" class="btn button">SELESAI</a></center></p>
				</div>
				</div>
			</div>
			<?php
			include('templates/footer.php');
			?>	
