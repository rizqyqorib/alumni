<?php
	include('templates/header.php');
?>

<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>
	<div class="container" style="margin-top: 100px;">
		<div class="row">
<!--profil alumni-->
			<div class="col-sm-8" style="margin-top: 20px;">
				<div class="box" style="padding:50px;">
			<ol class="breadcrumb">
				<li><a href="home.php">Home</a></li>
				<li><a href="tips.php">Tips-tips</a></li>
				<li class="active">Lakukan 5 Hal Ini Setelah Wisuda</li>
			</ol>
			<h2 style="margin-bottom: 50px;">Lakukan 5 Hal Ini Setelah Wisuda</h2>
					<center><img src="tips2.jpg" class="img-responsive" alt="..."></center>
							<br>
							<br>
						<p>Bagi kebanyakan mahasiswa wisuda adalah puncak kebahagiannya, namun kebahagian itu tidak berlangsung lama karena sesungguhnya dia sudah berada di lautan yang luas untuk menghadapi tantangan atau kehidupan yang sesungguhnya.<br><br>

						Lakukan hal ini setelah wisuda, kelak kamu akan wisuda juga dari Universitas Kehidupan dengan IPK SUKSES</p>
							<br>
						1. Travelling<br>	
						<p>Jalan – jalan akan menyegarkan pikiran dari penatnya kehidupan kampus, setelah 3 , 4 atau 5 tahun atau bahkan 6 tahun kuliah. Uang darimana untuk travelling ??? <br>

						Kamu gak harus jalan – jalan mahal, opsi yang murah namun sangat berkesan, kamu bisa camping di gunung atau pergi nyantai ke pantai. Tapi jangan sampai keasyikan travelling sehingga tanggung jawab lain menjadi terabaikan.</p>	
							<br>
						2. Lanjut Kuliah S2<br>	
						<p>Mumpung lagi semangat – semangatnya belajar, gak ada salahnya langsung lanjut kuliah S2. Apalagi kalau dapat beasiswa jangan disia-siakan.

						Karena setelah bekerja semangat untuk lanjut kuliah S2 akan berkurang, apa lagi setelah kenal uang atau gaji yang lumayan. Banyak beasiswa S2 yang bisa kamu cari, hanya saja kualifikasi atau persyaratannya lebih sulit dan lebih kompetitif, seperti LPDP dan lain sebagainya…</p>
							<br>
						3. Cari Kerja<br>	
						<p>Ini adalah kegiatan yang langsung dilakukan setelah wisuda, cari kerja mulai dari perusahaan A sampai perusahaan Z.

						Penting kamu katahui saat ini di Indonesia lapangan kerja SANGAT SEDIKIT apa lagi kamu tidak punya keahlian khusus ( softskill ). <br><br>

						Modal IPK tinggi belum tentu menjamin kamu untuk dapat kerja dengan cepat dan sesuai dengan passion kamu, apalagi kamu belum punya pengalaman dalam dunia kerja.

						Terlebih lagi saat ini banyak perusahaan yang sudah sangat selektif dalam mencari kandidat calon karyawan mereka.

						Persaingan dunia kerja saat ini sangat ketat, lemparlah lamaran kamu sebanyaknya ke Perusahaan atau Lembaga yang sesuai dengan kualifikasi kamu. Coba dengar lagu Iwan Fals – Sarjana Muda.

						</p>
							<br>
						4. Ikut Volunteer atau Sukarelawan<br>	
						<p>Saat ini banyak Yayasan atau Lembaga yang mencari volunteer. Mulai dari Yayasan yang bergerak dibidang lingkungan hidup, sosial budaya , kesehatan dan lain sebagainya.

						Ini adalah kesempatan buat kamu untuk mencari pengalaman & juga jaringan baru. Biasanya yang paling banyak diminati adalah volunteer di event – event besar olahraga seperti SEA GAMES, ASIAN GAMES, OLIMPIADE dan juga Piala Dunia.<br><br>

						Biasanya Yayasan memberikan fasilitas penuh seperti tempat tinggal, makan atau bahkan ada beberapa yang memberikan uang saku bagi para volunteer. Untuk event – event besar olahraga mereka terang – terangan memberikan uang saku.</p>	
							<br>
						5. Buka Usaha Sendiri atau Bangun Startup<br>	
						<p>Hanya sebagian kecil sarjana muda yang melakukan hal ini, karena modal utama dalam membuka usaha adalah mental pengusaha yang tidak pernah takut gagal.

						Pertanyaannya, modal darimana ?<br><br>

						Kamu bisa meneruskan usaha orang tua kamu, usaha apapun itu, namun banyak yang merasa malu untuk meneruskan usaha orang tuanya karena omongan orang atau tetangga.

						Kamu bisa meneruskan usaha tersebut dengan catatan kamu buat inovasi atau pembaharuan dan menjadikan usaha tersebut lebih maju lebih baik. Membuka tidak perlu modal yang saat besar bagi pemula, kamu hanya perlu ide kreatif dan bisa menangkap peluang yang ada. <br><br>

						Dunia startup sangat banyak digeluti mahasiswa bahkan sarjana muda saat ini. Membangun startup sama dengan membangun perusahaan sendiri, dan kamu harus bangun tim yang solid, konsisten, komitmen.<br>

						Kamu bisa ikut dalam Startup Event yang sangat ramai saat ini, membentuk tim di sana dan ikut dalam Incubator. Satu hal yang kamu harus ketahui, membangun startup membutuhkan kekonsistenan.</p>	
							<br>
						
				</div>	
			</div>
		<div class="col-sm-4" style="margin-top: 20px;">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="bea1.html">Panduan Beasiswa LPDP 2017</a></li>
					<li><a href="seminar1.html">Sentika : Universitas Islam Indonesia</a></li>
					<li><a href="loker1.html">Lowongan PT. Industri Kereta Api</a></li>
					<li><a href="loker2.html">Pertamina membuka kembali lowongan</a></li>
					<li><a href="loker3.html">Lowongan Global One Solusindo</a></li>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>
		</div>
	</div>
			<br>
			<br>
<?php
	include('templates/footer.php');
?>
