-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2018 at 02:23 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kampung-alumni`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumnitelkom`
--

CREATE TABLE `alumnitelkom` (
  `id_alumni` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `lokasi` varchar(30) NOT NULL,
  `angkatan` int(4) NOT NULL,
  `skripsi` varchar(30) NOT NULL,
  `perusahaan` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `kontak` varchar(30) NOT NULL,
  `narasi` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumnitelkom`
--

INSERT INTO `alumnitelkom` (`id_alumni`, `nama`, `jurusan`, `status`, `lokasi`, `angkatan`, `skripsi`, `perusahaan`, `username`, `kontak`, `narasi`) VALUES
(12345678, 'bryan alif satria bata', 'S1 Teknik Informatika', 'bekerja', 'jakarta', 2013, '-', 'PT Jonah Akbar', 'bryan', '082326054196', ''),
(13101121, 'Fasich Haryadi kirana', 'S1 Teknik Telekomunikasi', 'bekerja', 'CILACAP', 2013, '', 'PT Telkom Akses', 'haryadi', '0', ''),
(13101131, 'Nurul Fatonah', 'S1 Teknik Telekomunikasi', 'bekerja', 'purwokerto', 2013, '', 'ST3 Telkom Purwokerto', 'nurul', '0', ''),
(13102026, 'roslidia', 'S1 Teknik Informatika', 'belum bekerja', 'tegal', 2013, '-', '-', 'roslidia', '085642989024', ''),
(13102036, 'anggita ratih', 'S1 Teknik Informatika', 'belum bekerja', 'purbalingga', 2013, '-', '-', 'anggita', '081914958075', ''),
(13102040, 'dewi anata sari', 'S1 Teknik Informatika', 'bekerja', 'mojokerto', 2013, '-', 'robota', 'dewi', '085747775273', ''),
(13102043, 'elliyani pamupti', 'S1 Teknik Informatika', 'belum bekerja', 'paguyangan', 2013, '-', '-', 'elliyani', '082133846467', ''),
(13102053, 'puji lestari', 'S1 Teknik Informatika', 'bekerja', 'Brebes', 2013, '', '-', 'puji', '0', ''),
(13102057, 'reza maulana', 'S1 Teknik Informatika', 'bekerja', 'jakarta', 2013, '-', 'PT super sejahtera abadi', 'reza', '081226893335', ''),
(13102059, 'rizka dian pratama', 'S1 Teknik Informatika', 'belum bekerja', 'tangerang', 2013, '-', '-', 'rizka', '083891277447', ''),
(13102060, 'rizky syamsuri mati', 'S1 Teknik Informatika', 'wirausaha', 'cakung', 2013, '', 'PT Ovist jaya family', 'rizky', '08995068648', ''),
(13102063, 'syarifah camelia ambami', 'S1 Teknik Informatika', 'lanjut studi', 'Bandung', 2013, '', '-', 'camelia', '082242200863', 'dapet beasiswa dari kampus untuk lanjut kuliah S2 dibandung Telkom University'),
(13102066, 'yusuf ramli kirana', 'S1 Teknik Informatika', 'bekerja', 'jakarta', 2023, '', 'pt telkom access', 'yusuf', '0', ''),
(13102068, 'taat novan nugroho', 'S1 Teknik Informatika', 'belum bekerja', 'purwokerto', 2013, '', '', 'taat', '0', ''),
(14102077, 'rafi sovyan', 'S1 Teknik Informatika', 'belum bekerja', 'tasikmalaya', 2014, '-', '-', 'rafi', '082242698581', ''),
(23456789, 'lusi annisa sulis', 'S1 Teknik Informatika', 'bekerja', 'cikarang', 2013, '-', '-', 'lusi', '085725430489', '');

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

CREATE TABLE `saran` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(20) NOT NULL,
  `saran` varchar(1000) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `judul` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id`, `nama`, `email`, `saran`, `kategori`, `foto`, `judul`) VALUES
(10, 'bebas', 'email@email.com', 'bebas', 'Beasiswa', '12366418_1105054759507702_4326317180912074750_n.jpg', 'test'),
(12, 'rizqy', 'rizkyqorib@gmail.com', 'Mengingat pesatnya kebutuhan tenaga kerja sebagai Drive Test Engineer di industri Telekomunikasi maka PT. Global One Solusindo (G1) mengundang ALUMNI dari ST3 Telkom Purwokerto untuk mengikuti pelatihan intensif selama 5 hari di Jakarta sebagai Drive Test Engineer. Selesai pelatihan akan dilaksanakan seleksi, peserta yang lulus ditawarkan kontrak oleh pihak G1 sebagai karyawan dengan posisi sebagai Drive Test Engineer selama 1 thn.\r\n\r\nPelatihan diberikan secara GRATIS. Bagi para siswa yang mengikuti pelatihan ini secara full selama 5 hari dan lulus seleksi maka diberikan pergantian uang makan sebesar Rp.50.000/hari. Setelah seleksi maka kami akan kami tawarkan kontrak 1 thn sebagai DT Engineer dengan penempatan di lokasi-lokasi projek G1 di seluruh Indonesia.\r\n\r\nuntuk pendaftaran bisa mengisi di link bit.ly/BAWOR atau kirim langsung lamaran ke sri.andayani@globalonesolusindo.com KESEMPATAN UNTUK IKUT TRAINING DIBUKA SETIAP MINGGU !\r\n\r\n', 'Lowongan kerja', 'loker3.png', 'Lowongan Global One Solusindo'),
(13, 'qorib', 'qorib@gmail.com', 'Seminar Nasional (SEMNAS) Informatika UII adalah sebuah seminar yang menggabungkan tiga buah seminar yang sebelumnya dilaksanakan secara terpisah oleh Jurusan Teknik Informatika UII yaitu : Seminar Nasional Aplikasi Teknologi Informasi (SNATi), Seminar Nasional Informatika Medis (SNIMed), dan Hacking and Digital Forensics Exposed (H@dfex).\r\n\r\n\r\nTahun 2017 ini merupakan kedua kalinya ketiga seminar tersebut dilaksanakan secara bersamaan dengan nama Seminar Nasional Informatika UII. Dengan penyatuan ketiga seminar tersebut diharapkan dapat menghemat sumber daya dan pembiayaan kegiatan, tetapi tetap memberikan pelayanan terbaik bagi peserta. Selain itu peserta salah satu seminar juga mendapatkan kesempatan untuk bisa melihat pelaksanaan dari seminar lainnya.\r\n\r\n\r\nTahun ini tema untuk masing-masing seminar adalah â€œInternet Of Things Untuk Pembangunan Kota Cerdasâ€ untuk SNATI, â€œOntology Sebagai Pendukung dalam Pemberian Terapi Medis yang Tepatâ€ untuk SNIMed, dan â€œExplore The New C', 'Kegiatan', 'seminar1.png', 'Sentika : Universitas Islam Indonesia');

-- --------------------------------------------------------

--
-- Table structure for table `testiomi`
--

CREATE TABLE `testiomi` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(20) NOT NULL,
  `saran` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testiomi`
--

INSERT INTO `testiomi` (`id`, `nama`, `email`, `saran`) VALUES
(1, 'naoomi', 'naoomi@gmail.com', 'baru aja mau cabut'),
(2, 'ayu', 'ayu@gmail.com', 'pergi kesana'),
(3, 'asdsadasdasd', 'asdasdas', 'Komentar');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(15) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `level_user` enum('admin','alumni') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `level_user`) VALUES
(1, 'raja', 'raja', 'natavonta', 'admin'),
(9, 'yusuf ramli', 'yusuf', '13102066', 'alumni'),
(10, 'fasich haryadi', 'haryadi', '13101121', 'alumni'),
(11, 'Rizqy Fathan Qorib', 'rizqy', '13102061', 'alumni'),
(13, 'Nurul Fatonah', 'nurul', '13101131', 'alumni'),
(14, 'Nurul Fatonah', 'nurul', '13101131', 'alumni'),
(15, 'syarifah camelia ambami', 'camelia', '13102063', 'alumni'),
(17, 'taat novan nugroho', 'taat', '13102027', 'alumni'),
(18, 'rizky syamsuri', 'rizky', '13102060', 'alumni'),
(20, 'bryan alif satria', 'bryan', '12345678', 'alumni'),
(21, 'rizka dian pratama', 'rizka', '13102059', 'alumni'),
(22, 'puji lestari', 'puji', '13102053', 'alumni'),
(23, 'lusi annisa', 'lusi', '12345678', 'alumni'),
(24, 'rafi sovyan', 'rafi', '14102077', 'alumni'),
(25, 'reza maulana', 'reza', '13102057', 'alumni'),
(26, 'dewi anata sari', 'dewi', '13102040', 'alumni'),
(27, 'anggita ratih', 'anggita', '13102036', 'alumni'),
(28, 'elliyani pamupti', 'elliyani', '13102043', 'alumni'),
(29, 'roslidia', 'roslidia', '13102026', 'alumni');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumnitelkom`
--
ALTER TABLE `alumnitelkom`
  ADD PRIMARY KEY (`id_alumni`);

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testiomi`
--
ALTER TABLE `testiomi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumnitelkom`
--
ALTER TABLE `alumnitelkom`
  MODIFY `id_alumni` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23456790;
--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `testiomi`
--
ALTER TABLE `testiomi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
