$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if(scroll == 0){
    	$(".navbar-default").css('box-shadow', '0 0px 0px 0px rgba(0,0,0,0.06), 0 0px 0px 0 rgba(0,0,0,0.06), 0 0px 0px 0 rgba(0,0,0,0.08)');
    	$(".navbar").css('background-color', 'transparent');
    	$(".navbar-default .navbar-nav>li>a").css('color', '#fff');
    	$(".navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus").css('color', '#fff');
    	$(".login").css("color", "#fff");
    	$(".navbar-brand").html('<img src="../dist/img/logoputih.png" class="img-responsive" style="width: 50%;">');
        $(".login:hover").css('box-shadow', 'none');
    }else{
    	$(".navbar-default").css('box-shadow', '0 2px 4px -1px rgba(0,0,0,0.06), 0 4px 5px 0 rgba(0,0,0,0.06), 0 1px 10px 0 rgba(0,0,0,0.08)');
    	$(".navbar").css('background-color', '#fff');
    	$(".navbar-default .navbar-nav>li>a").css('color', '#000');
    	$(".navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus").css('color', '#E62129');
    	$(".login").css("color", "#fff");
    	$(".navbar-brand").html('<img src="../dist/img/logo.png" class="img-responsive" style="width: 50%;">');
        $(".login:hover").css('box-shadow', '-1px 6px 14px -2px rgba(0,0,0,0.4)');
    }
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})