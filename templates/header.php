<!DOCTYPE html>
<html lang="en">
  <head>
  	<style>
  		.button {
    background-color: #e62129;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
	}

  	</style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Portal Alumni - Beranda</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Poppins" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="dist/datatables/css/datatables.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="dist/datatables/DataTables-1.10.16/css/dataTables.bootstrap.min.css"> -->

    <link rel="stylesheet" type="text/css" href="dist/custom/css/rifqi.css">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="dist/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<!--NAVBAR-->
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
	    <div class="navbar-header" >
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span> 
	      </button>
	      
	      <a class="navbar-brand" style="position: absolute;" href="home.php"><img src="dist/img/logoputih.png" class="img-responsive" style="width: 50%;"></a> 
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
	      <ul class="nav navbar-nav" style="margin-left: 12%;">
	        <li class="active"><a href="home.php">Beranda</a></li>
	        <li><a href="tentang_kami.php">Tentang Kami</a></li>
	        <li class="dropdown">
		        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Berita
		        <span class="caret"></span></a>
		        <ul class="dropdown-menu">
		          <li><a href="bea.php">Beasiswa</a></li>
		          <li><a href="kegiatan.php">Kegiatan</a></li>
		          <li><a href="tips.php">Tips</a></li>
		        </ul>
		    </li>
		    <li><a href="loker.php">Lowongan Pekerjaan</a></li> 
	      </ul>
	      <div class="nav navbar-nav navbar-right" style="margin-right: 50px;">
        <li><a href="bantuan.php">Butuh Bantuan?</a></li> 
	      <li style="padding: 15px;"><a class="login" style="padding: 0px 30px; border-radius: 25px; background-color: #E62129; color: #fff;" data-toggle="modal" data-target="#login">Sign in</a></li>
	      </div	>	
	    </div>
	  </div>
	</nav>

<!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">

        	<i data-dismiss="modal" aria-label="Close" class="material-icons ikon" style="margin-bottom: 15px;">arrow_back</i>

        <div class="content-modal">
        	<h3>Login</h3>
        	<p style="margin-bottom: 60px;">Fill out the from below to get started</p>

		<form class="login-1" action="proseslogin.php" method="POST" autocomplete="off">
		  <div class="form-group">
		  	<div class="input-group">
		      <div class="input-group-addon"><i class="material-icons">supervisor_account</i></div>
		      <input name="username" type="text" class="form-control" id="exampleInputAmount" placeholder="Username">
		    </div>
		  </div>
		  <div class="form-group">
		  	<div class="input-group">
		      <div class="input-group-addon"><i class="material-icons">vpn_key</i></div>
		      <input name="password" type="password" class="form-control" id="exampleInputAmount" placeholder="Password">
		    </div>
		  </div>
		  <button type="submit" name="login" class="btn btn-default button-login">Submit</button>
		</form>

        </div>
      </div>
    </div>
  </div>
</div>