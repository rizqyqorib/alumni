<?php
	include('templates/header.php');
?>
<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>	
<!--BERITA-->
	<div class="container" style="margin-top: 100px;">
		<div class="col-sm-8" style="margin-top: 20px;">
			<div class="box" style="padding: 50px;">
								<h2>Kegiatan / Seminar</h2>
								<hr>
			<br>
					
					<div class="media-left">
							<a href="kegiatan1.php">
								<img src="seminar1.png" width="200px" height="150px">
							</a>
					</div>
					<div class="media-body">
							<a href="kegiatan1.php">
								<h3 class="media-heading">Sentika : Universitas Islam Indonesia</h3>
									<br>
							</a>
								<p>Seminar Nasional (SEMNAS) Informatika UII adalah sebuah seminar 
								yang menggabungkan tiga buah seminar yang sebelumnya dilaksanakan 
								secara terpisah oleh Jurusan Teknik Informatika.</p>
						<br>
					</div>
						<hr>
			<br>
					<div class="media-left">
							<a href="berita1.html">
								<img src="loker1.jpg" width="200px" height="150px">
							</a>
						</div>
					<div class="media-body">
							<a href="berita1.html">
								<h3 class="media-heading">ST3 Telkom kembali membuka kesempatan menjadi dosen tetap</h3>
									<br>
							</a>
								<p>Sekolah Tinggi Teknologi Telematika Telkom Purwokerto kembali membuka kesempatan kepada
								putra putri terbaik untuk menjadi calon dosen
								tetap periode Mei 2017. Untuk keterangan lebih lanjut dapat di lihat di bawah ini</p>
						<br>
					</div>
						<hr>
			<br>
					<div class="media-left">
							<a href="berita1.html">
								<img src="loker1.jpg" width="200px" height="150px">
							</a>
						</div>
					<div class="media-body">
							<a href="berita1.html">
								<h3 class="media-heading">ST3 Telkom kembali membuka kesempatan menjadi dosen tetap</h3>
									<br>
							</a>
								<p>Sekolah Tinggi Teknologi Telematika Telkom Purwokerto kembali membuka kesempatan kepada putra putri terbaik untuk menjadi calon dosen
								tetap periode Mei 2017. Untuk keterangan lebih lanjut dapat di lihat di bawah ini</p>
						<br>
					</div>
						<hr>
					<nav>
						<ul class="pager">
							<li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Sebelumnya</a></li>
							<li class="next"><a href="#">Selanjutnya <span aria-hidden="true">&rarr;</span></a></li>
						</ul>
					</nav>
			</div>
		</div>		
<!-- RIGHT PLACE -->	
		<div class="col-sm-4" style="margin-top: 20px;">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="bea1.html">Panduan Beasiswa LPDP 2017</a></li>
					<li><a href="seminar1.html">Sentika : Universitas Islam Indonesia</a></li>
					<li><a href="loker1.html">Lowongan PT. Industri Kereta Api</a></li>
					<li><a href="loker2.html">Pertamina membuka kembali lowongan</a></li>
					<li><a href="loker3.html">Lowongan Global One Solusindo</a></li>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>
	</div>
<?php
	include('templates/footer.php');
?>
