<?php 
	include('templates/header.php');
?>

<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>	

	<div class="container" style="margin-top: 100px;">
		<div class="col-sm-8" style="margin-top: 20px;">
			<div class="box" style="padding: 50px;">
				<ol class="breadcrumb">
				<li><a href="home.php">Home</a></li>
				<li class="active">Tips-tips</li>
			</ol>
				<h2>Tips - tips</h2>
				<hr>
							<br>
					<div class="media-left">
							<a href="tips1.php">
								<img src="tips.png" width="200px" height="150px">
							</a>
						</div>
					<div class="media-body">
							<a href="tips1.php">
								<h3 class="media-heading">3 Tips Agar Anda Betah Kerja di Kantor</h3>
									<br>
							</a>
								<p>Tidak mudah untuk membuat rasa nyaman pada sebuah pekerjaan hal itu yang harus Anda pikiran saat Anda menjalani pekerjaan yang Anda jalani 
								sekarang.</p>
						<br>
					</div>
						<hr>
			<br>		
					<div class="media-left">
							<a href="tips2.php">
								<img src="tips1.jpg" width="200px" height="150px">
							</a>
						</div>
					<div class="media-body">
							<a href="tips2.php">
								<h3 class="media-heading">9 cara supaya langsung kerja setelah wisuda</h3>
									<br>
							</a>
								<p>Setelah berhasil mengantongi gelar sarjana perasaan lega pasti langsung kamu rasakan. Setidaknya perasaan mampu menyelesaikan tanggung jawab sebagai mahasiswa kamu miliki.</p>
						<br>
					</div>
						<hr>
			<br>
					<div class="media-left">
							<a href="tips3.php">
								<img src="tips2.jpg" width="200px" height="150px">
							</a>
						</div>
					<div class="media-body">
							<a href="tips3.php">
								<h3 class="media-heading">Lakukan 5 Hal Ini Setelah Wisuda</h3>
									<br>
							</a>
								<p>Bagi kebanyakan mahasiswa wisuda adalah puncak kebahagiannya, namun kebahagian itu tidak berlangsung lama karena sesungguhnya dia sudah berada di lautan yang luas</p>
						<br>
					</div>
						<hr>
			<br>
					
<!-- HALAMAN -->	
<nav>
						<ul class="pager">
							<li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Sebelumnya</a></li>
							<li class="next"><a href="#">Selanjutnya <span aria-hidden="true">&rarr;</span></a></li>
						</ul>
					</nav>
			</div>
		</div>
<!-- RIGHT PLACE -->		
<div class="col-sm-4" style="margin-top: 20px;">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="bea1.html">Panduan Beasiswa LPDP 2017</a></li>
					<li><a href="seminar1.html">Sentika : Universitas Islam Indonesia</a></li>
					<li><a href="loker1.html">Lowongan PT. Industri Kereta Api</a></li>
					<li><a href="loker2.html">Pertamina membuka kembali lowongan</a></li>
					<li><a href="loker3.html">Lowongan Global One Solusindo</a></li>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>
	</div>
<?php
	include('templates/footer.php');
?>
