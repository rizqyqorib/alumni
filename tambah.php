<?php
	include('templates/header.php');
?>
<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>
		
	
<!--BERITA-->
	<div class="container" style="margin-top: 100px;">
		<div class="col-sm-8 box" style="padding: 50px;">
				<h2>Tambah Berita</h2>
				<br>
			<div class="col-sm-6">	
				<form action="simpanBerita.php" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label for="username">Nama</label>
						<input type="text" name="username" class="form-control" placeholder="Nama">
					</div>
			</div>			
			<div class="col-sm-6">			
						<div class="form-group">
							<label for="email">Email</label>
							<input type="text" name="email" class="form-control" placeholder="Email">
						</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
							<label for="sel1">Kategori</label>
								<select class="form-control" id="sel1" name="kategori">
									<option>Beasiswa</option>
									<option>Kegiatan</option>
									<option>Lowongan kerja</option>
									<option>Tips-tips</option>
								</select>
								<br>
				</div>
			</div>
			<div class="col-sm-6">
							<label for="sel1">Pilih Gambar </label>	
									<div id="image-preview-div" style="display: none">
										<label for="exampleInputFile">Selected image:</label>
										<br>
										<img id="preview-img" src="noimage">
									</div>
									<div class="form-group">
										<input type="file" name="file" id="file" required>
									</div>
			</div>
			<div class="col-sm-12">			
						<div class="form-group">
							<label for="judul">Judul Berita</label>
							<input type="text" name="judul" class="form-control" placeholder="Judul">
						</div>
			</div>	
			<div class="col-sm-12">	
				<div class="form-group">
				  <label for="comment">Berita:</label>
				  <textarea class="form-control" name="saran" rows="5" id="comment"></textarea>
				</div>
			</div>
			<div class="col-sm-12">	
					<button type="submit" class="button" name="submit">Kirim</button>
			</div>	
		</div>
	</form>
		<div class="col-sm-4">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="bea1.html">Panduan Beasiswa LPDP 2017</a></li>
					<li><a href="seminar1.html">Sentika : Universitas Islam Indonesia</a></li>
					<li><a href="loker1.html">Lowongan PT. Industri Kereta Api</a></li>
					<li><a href="loker2.html">Pertamina membuka kembali lowongan</a></li>
					<li><a href="loker3.html">Lowongan Global One Solusindo</a></li>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>
	</div>
<?php
	include('templates/footer.php');
?>
