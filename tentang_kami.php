<?php
	include('templates/header.php');
?>
<!--kontak-->
<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>
	<div class="container" style="margin-top: 120px;">
<!--profil alumni-->
		<div class="col-sm-8">
			<div class="box" style="padding: 25px;">
				<h2>Tentang Kami</h2>
				<hr>
				<h3>Apa itu Website Portal Alumni ITTP</h3>
				Website portal alumni adalah sebuah website yang dirancang oleh unit kerja Carrer development center (CDC) Institut Teknologi Telkom Purwokerto (ITTP). Website ini menyediakan Beberapa informasi seperti lowongan pekerjaan, beasiswa, dan kegiatan lainnya yang ditujukan untuk alumni ITTP. Sebuah website yang diharapkan dapat berfungsi sebagai penunjang interaksi sesama alumni untuk saling berbagi informasi, pengalaman maupun cerita.</p>
				<h3>CDC ITTP</h3>	
				Carrer Development Center (CDC). CDC adalah sebuah unit kerja ITTP yang berfokus dalam suatu pelayanan karir terpadu yang bergerak dalam rangka membangun dan mengembangkan profesionalisme lulusan atau alumni dari ITT Purwokerto. CDC ITTP mempunyai dua fungsi utama, yakni fungsi persiapan dan fungsi peninjauan.<br><br>
			        	<b>Fungsi Persiapan</b><br>
			        	Fungsi persiapan dilakukan semasa perkuliahan sedang berlangsung dan ditujukan untuk membentuk karakter lulusan atau alumni. fungsi persiapan merupakan bagian yang memfasilitasi dan menjembatani sebelum lulusan memasuki dunia kerja dalam membangun sofskills lulusan dengan cara mengadakan seminar atau pelatihan semasa masih menjadi mahasiswa aktif ITTP. dan pelatihan ini dibuka untuk umum, dalam artian, bagi alumni juga diperbolehkan untuk mengikutinya. <br><br> 
			        	<b>Fungsi Peninjauan</b><br>
			        	Fungsi Peninjauan adalah pengumpulan feedback dari pihak industri mengenai performansi alumni selama bekerja. bertujuan untuk mengurangi gap antara industri terhadap kurikulum pengajaran ITTP. hasil feedback ini yang nanti akan dikirim ke masing-masing prodi ITTP untuk  dijadikan evaluasi dalam perancangan kurikulum baru sesuai dengan kebutuhan pihak industri dalam memperoleh SDM yang berkarakter. <br><br>
			        	<b>Program Kerja</b><br>
			        	Salah satu program kerja CDC adalah membangun sebuah Wesbite dimana menyediakan informasi lowongan pekerjaan, beasiswa dan info kegiatan lainnya mengenai alumni. Informasi-informasi ini akan terus di update menyesuaikan kebutuhan dari pihak industri dan juga lulusan nya. <br><br>
			    	
			</div>
		</div>
		<div class="col-sm-4">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<?php
						include ("koneksi.php");
						$query = "Select * from saran order by id desc limit 5";
						$data = mysql_query($query);
						while($hasil = mysql_fetch_array($data)){
					?>
					<li><a href="detailberita.php?id="<?php echo $hasil['id'];?></a></li>
					<?php
				}
					?>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>	
	</div>						
			
<?php
	include('templates/footer.php');
?>
