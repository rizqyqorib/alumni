<?php
	include('templates/header.php');
?>

<div style="background-color: #E62129; width: 100%; height: 50vh; position: absolute; top: 0px;">

</div>
	<div class="container" style="margin-top: 100px;">
		<div class="row">
<!--profil alumni-->
			<div class="col-sm-8" style="margin-top: 20px;">
				<div class="box" style="padding:50px;">
			<ol class="breadcrumb">
				<li><a href="home.php">Home</a></li>
				<li><a href="tips.php">Tips-tips</a></li>
				<li class="active">9 cara supaya langsung kerja setelah wisuda</li>
			</ol>
			<h2 style="margin-bottom: 50px;">Cuma 9 Cara Ini yang Bisa Membuatmu Langsung Bekerja Setelah Wisuda</h2>
					<center><img src="tips.png" class="img-responsive" alt="..."></center>
							<br>
							<br>
						<p>Setelah berhasil mengantongi gelar sarjana perasaan lega pasti langsung kamu rasakan. Setidaknya perasaan mampu menyelesaikan tanggung jawab sebagai mahasiswa kamu miliki. Namun sayangnya perasaan itu tidak akan berlangsung lama, karena beberapa waktu ke depan pertanyaan, “Sekarang kerja di mana?” akan sering mampir ke telinga. Agar kamu bisa langsung kerja setelah lulus, Berikut cara mendapat kerja secepatnya:</p>
							<br>
						1. Berburu kesempatan kerja di job fair. Siapa tahu peluangmu mendapat pekerjaan datang dari sana<br>	
						<p>Kamu akan sulit mendapat pekerjaan kalau hanya duduk di rumah saja. Karenanya cobalah lebih aktif mencari peluang, misalnya dengan datang ke job fair. Sekarang ini di berbagai kota bahkan kampus, event job fair sering kali diadakan. Di acara tersebut kamu dapat melamar pekerjaan mulai dari perusahaan lokal, multinasional, bahkan internasional. Selain itu, di job fair kamu juga bisa menemukan beragam kesempatan menarik, misalnya dengan mengikuti program management trainee di perusahaan bergengsi. </p>	
							<br>
						2. Ikutan magang agar kamu memiliki pengalaman sebagai nilai jual<br>	
						<p>Salah satu syarat yang membuat kamu “bersinar” di mata pemberi pekerjaan adalah pengalaman. Bahkan bisa dikatakan pengalaman berperan lebih besar untuk karir dibandingkan nilai tinggi. Untuk itu kamu harus bergerak cepat mengumpulkan pengalaman yang salah satunya bisa didapatkan melalui magang. Kamu dapat mengajukan permohonan magang ke perusahaan yang diinginkan. Jika performance-mu baik saat magang bukan gak mungkin loh, kamu malah ditawari bergabung di sana. </p>
							<br>
						3. Bergabung dalam program management trainee<br>	
						<p>Kamu pasti pernah kan mendengar istilah management trainee (MT)? Program tersebut sekarang ini dikembangkan oleh banyak perusahaan dan bertujuan untuk mencari bibit unggul berusia muda. Salah satu perusahaan bergengsi yang sedang membuka program MT adalah PT Coca-Cola Amatil Indonesia (CCAI). CCAI merupakan perusahaan minuman non alkohol terbesar di Asia Pasifik. Produk-produk seperti Coca Cola, Fanta, dan Frestea yang kamu lihat setiap hari itu diproduksi oleh perusahaan ini. </p>
							<br>
						4. Perluas jaringan pertemanan. Siapa tahu kesempatanmu bekerja datang dari teman baru<br>	
						<p>Jangan takut untuk memulai pertemanan dengan orang baru. Ketika setelah lulus misalnya kamu pergi traveling, jangan asyik sendiri. Jadikan hal tersebut sebagai ajang menambah koneksi. Dengan jaringan pertemanan yang luas biasanya kesempatan untuk mendapat pekerjaan terbuka makin lebar.</p>	
							<br>
						5. Rajin-rajinlah berkunjung ke website penyedia lowongan pekerjaan<br>	
						<p>Hindari menggunakan smartphone atau laptop hanya untuk browsing musik atau video favoritmu saja. Gunakan gadget untuk membuatmu cepat mendapat pekerjaan, misalnya dengan mengakses website penyedia lowongan pekerjaan. Dengan mengunjungi website seperti ini kamu dapat memantau perusahaan mana saja yang sedang membuka lowongan. Jika telah menemukan tawaran yang sesuai kirimkan lamaranmu secepatnya ya!</p>	
							<br>
						6. Minta bantuan teman atau kenalan yang sudah bekerja memberi tahumu informasi kalau ada lowongan pekerjaan tersedia<br>	
						<p>Seandainya kamu memiliki teman yang sudah bekerja, tidak ada salahnya meminta tolong dia untuk memberi informasi mengenai lowongan pekerjaan. Biasanya bantuan dari teman efektif membantumu mendapat informasi yang paling update. Apalagi kalau temanmu ini mengenalmu dengan baik dia bisa saja memberi rekomendasi positif sehingga peluangmu diterima makin besar.</p>
						7. Kirimkan lamaran pekerjaan sebanyak-banyaknya
							<br>
						<p>Aksi gerilya juga dapat kamu lakukan saat sedang mencari pekerjaan setelah lulus. Gerilya yang IDNtimes maksud adalah kamu mengirimkan lowongan sebanyak-banyaknya. Walaupun mungkin perusahaan yang kamu tuju tidak terlihat sedang membuka lowongan pekerjaan, kamu kirimkan saja lamaranmu. Siapa tahu saat perusahaan tersebut membutuhkan karyawan baru kamu langsung dipanggil.</p>
						8. Kalau kamu suka dengan dunia kreatif, buat website yang berisi portofoliomu
							<br>
						<p>Apakah kamu berniat kerja di industri kreatif? Jika jawabannya ya, segera buat website yang berisi portofoliomu. Tujuan membuat website ini adalah agar makin banyak orang mengetahui karya yang kamu miliki. Di era digital seperti sekarang kamu bisa memasarkan diri dengan cara kreatif macam ini.</p>
						9. Ikuti laman atau akun di media sosial yang berisi lowongan pekerjaan
							<br>
						<p>Cara terakhir yang bisa kamu lakukan agar bisa cepat mendapat kerja adalah mengikuti grup atau akun di media sosial yang berbagai info lowongan pekerjaan. Kalau kamu mencari sekarang ada banyak banget kok akun yang dikhususkan untuk keperluan tersebut. Tapi sebelum kamu bergabung di grup atau follow akun macam ini cek dulu ya kredibilitasnya. Jangan sampai kamu tertipu lowongan pekerjaan karena bergabung di akun atau web yang kredibilitasnya tidak bisa dipercaya.</p>
							<br>
						<p>Walaupun mendapatkan pekerjaan sekarang ini terbilang cukup berat, bukan berarti kamu boleh langsung menyerah. Semoga 9 cara di atas dapat membantumu meraih karir cemerlang ya!</p>	
							<br>
				</div>	
			</div>
		<div class="col-sm-4" style="margin-top: 20px;">
			<div class="box" style="padding: 5px 25px;">
				<h3>Artikel Terbaru</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="bea1.html">Panduan Beasiswa LPDP 2017</a></li>
					<li><a href="seminar1.html">Sentika : Universitas Islam Indonesia</a></li>
					<li><a href="loker1.html">Lowongan PT. Industri Kereta Api</a></li>
					<li><a href="loker2.html">Pertamina membuka kembali lowongan</a></li>
					<li><a href="loker3.html">Lowongan Global One Solusindo</a></li>
				</ul>
			</div>

			<div class="box" style="padding: 5px 25px; margin-top: 25px;">
				<h3>Quick Link</h3>
				<hr>
				<ul class="list-unstyled" style="margin-bottom: 30px;">
					<li><a href="carialumni1.php">Pencarian Alumni</a></li>
					<li><a href="tambah.html">Tambahkan Berita</a></li>
				</ul>
			</div>
		</div>
		</div>
	</div>
			<br>
			<br>
<?php
	include('templates/footer.php');
?>
